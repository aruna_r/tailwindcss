module.exports = {
  mode:"jit",
  content: ["public/index.html"],
  theme: {
    extend: {
      colors:{
        menugold:'#B78B2E',
        menufont:'#606060',
     },
     fontFamily: {
      'disp': ['Montserrat'],
    },
  
    },
  },
  plugins: [],
}
